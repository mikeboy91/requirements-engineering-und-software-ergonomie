\documentclass{llncs}
\bibliographystyle{splncs}
\usepackage{url}
\usepackage{graphicx}
\usepackage{float}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[german]{babel}

\begin{document}
  \author{Michael Woywod}
  \title{Requirements-Engineering und Software-Ergonomie – wie passt das zusammen?}
  \institute{Fachhochschule Erfurt University Of Applied Sciences}
  \maketitle
  %
  \begin{abstract}
    Dieses Paper beschäftigt sich mit der Frage ob und wie Requirements-Engineering und
    Software-Ergonomie zusammen passen. Beleuchtet werden vor allem die Berührungspunkte sowie
    Integrationsansätze.
  \end{abstract}
  %
  \section{Grundlagen}
  %
  Im ersten Teil eine Erklärung der zwei elementaren Begrifflichkeiten Requirements-Engineering
  und Software-Ergonomie.
  %
  \subsection{Was ist Requirements-Engineering?}
  %
  Im Allgemeinen bezeichnet Requirements-Engineering "das ingenieurmäßige Festlegen der
  Anforderungen an ein System; [...] im Software Engineering auf
  Softwareprodukte"\cite{gabler:requirements_engineering}.
  Im Requirements-Engineering gibt es drei Arten von Anforderungen: funktionale Anforderungen,
  Qualitätsanforderungen sowie Rahmenbedingungen.
  \newline
  Die funktionalen Anforderungen beschreiben, was die Software oder die einzelnen Komponenten
  tun sollen und was nicht. Diese Anforderungen werden typischerweise in verschiedenen
  Perspektiven formuliert\cite{pohl:re}.
  \newline
  Qualitätsanforderungen sind Anforderungen an "die Güte des Softwaresystems oder einzelner
  Systembestandteile. Beispiele für solche Anforderungen sind Zuverlässigkeit, Benutzbarkeit
  und Performance (aus der Sicht der Systembenutzer) sowie Änderbarkeit und Portabilität
  (aus der Sicht der Entwickler)"\cite{edw:requirements_engineering}.
  \newline
  Die "Rahmenbedingungen sind Anforderungen, die die Realisierungsmöglichkeiten für ein
  Softwaresystem einschränken und nur schwer oder gar nicht geändert werden
  können"\cite{edw:requirements_engineering}. Sie lassen sich in die Bereiche Technologisch,
  Organisatorisch, Rechtlich und Ethisch unterteilen.
  %
  \subsection{Was ist Software-Ergonomie?}
  %
  Beschäftigt sich die traditionelle Ergonomie mit "der optimalen Gestaltung von
  Arbeitssystemen in Bezug auf die Abstimmung zwischen Mensch, Maschine und
  Arbeitswelt"\cite{gabler:ergonomie}, so beschäftigt sich die Software-Ergonomie im Speziellen
  gezielt mit der "Anpassung der Eigenschaften eines Dialogsystems an die psychischen und
  physischen Eigenschaften der damit arbeitenden Menschen"\cite{h_it:software_ergonomie}.
  %
  \section{Integration von Software-Ergonomie in das Requirements-Engineering}
  %
  Da im Requirements-Engineering die Anforderungen an eine Software gestellt werden, lässt sich
  so natürlich auch formal recht trivial eine ergonomische Gestaltung der Software als
  Anforderung formulieren. Umsetzen lässt sich dies durch benutzer- und aufgabenorientierte
  Modellierung einerseits, durch stetige Benutzerbeteiligung andererseits. Benutzerbeteiligung
  meint dabei, die Einbeziehung echter (künftiger) Nutzer im Bezug auf gewisse Teilbereiche des
  Designs. Aus den Angaben der künftigen Benutzer sollten dann Prototypen entstehen, welche dann
  von den Nutzern getestet werden. Abgegebenes Feedback sollte in die 
  Weiterentwicklung/Verbesserung der ergonomischen Gestaltung einbezogen werden.
  %
  \subsection{Benutzeranalyse bzw. Zielgruppenanalyse}
  %
  Egal ob Individualsoftware oder Software für den Massenmarkt, die Software richtet sich an
  spezielle Nutzergruppen. Noch bevor mit der Entwicklung angefangen wird, sollte vorher bereits
  eine Zielgruppenanalyse durchgeführt werden. In der Regel wird mit der Wahl geeigneter
  Segmentierungskriterien begonnen. Je nach Softwaretyp und Kundentyp unterscheiden sich
  diese Kriterien jedoch. Für Geschäftskunden sind beispielsweise Lebensdauer, Supportverträge
  oder Integration in bereits bestehende Systeme wichtig, während bei Privaten Kunden soziale,
  finanzielle und auch psychologische Faktoren wichtig sind.
  \newline
  Sobald geeignete Segmentierungskriterien gefunden sind, werden als nächstes Informationen
  über die entsprechenden Marktteilnehmer ermittelt.
  \newline
  Zum Schluss werden mit Hilfe dieser Informationen Zielgruppen definiert.\cite{moser:ux_design}
  %
  \subsection{Anwenderbeteiligung}
  %
  Sind entsprechende Zielgruppen gefunden, sollten sie anteilig und gleichberechtigt integraler
  Bestandteil der Software-Entwicklung sein. In einem zyklischem und kontinuierlich laufendem
  Entwicklungsprozess testen die Anwender verschiedene Prototypen und geben Feedback ab.
  Dieses Feedback fließt in den nächsten Prototypen ein und der Zyklus beginnt von
  vorn.\cite{brautigam:grundwissen_se}
  %
  \section{Problemstellungen und Herausforderungen}
  %
  Diese formal recht simple Integration bringt allerdings auch einige, nicht zu
  unterschätzende Probleme und Herausforderungen mit sich. Einige dieser Problemstellungen
  werden im Folgenden besprochen.
  %
  \subsection{Verwendung unpassender Entwicklungsmodelle}
  %
  Obwohl inzwischen der Großteil der Softwareunternehmen auf Agile Softwareentwicklung setzen,
  gibt es dennoch einen kleinen Teil, der immer noch auf lineare Entwicklungsmodelle
  setzt\cite{vo:survey}.
  %
  \begin{figure}[h]
    \includegraphics[width=\textwidth]{img/wasserfall.jpeg}
    \caption{Wasserfall Modell als klassische Methode in der Softwareentwicklung.\newline
    Bildquelle: http://www.html-lernen.de/2013/02/software-entwicklung-mit-dem-wasserfall-modell/}
    \label{fig:wasserfall_modell}
  \end{figure}
  %
  Wie in Abbildung \ref{fig:wasserfall_modell} zu sehen ist, folgt eine Phase auf die Nächste,
  ohne einen Weg zurück. Dies ist insofern problematisch, als dass auf Änderungen oder neue
  Anforderungen kaum oder gar nicht reagiert werden kann. Eine ergonomische Gestaltung der
  zu entwickelnden Software erfordert allerdings eine schnelle Reaktion auf gegebenes Feedback.
  So sollte es schon während der Entwicklung nicht nur Funktionstests, sondern auch
  ergonomische Tests, das Design betreffend, geben. Die dadurch gewonnenen Erkenntnisse
  sollten schnell in den Entwicklungsprozess einfließen.
  %
  \subsection{Programmierer kennt die Arbeitsabläufe nicht aus eigener Erfahrung}
  %
  Zwar werden die einzelnen Anforderungen sehr detailliert besprochen und dokumentiert,
  jedoch kennen die Programmierer und Designer die entsprechenden Arbeitsabläufe nicht aus
  eigener Erfahrung (Bsp: Buchhaltungssoftware). So besteht die Gefahr, dass eigentlich
  wichtige und häufig benutzte Funktionen sehr versteckt oder nur umständlich zu erreichen
  sind. Wie bereits erwähnt, lässt sich mit passend gewählten Entwicklungsmodellen und
  Tests mit fachkundigen, künftigen Anwendern noch während der Entwicklung dagegen steuern.
  %
  \subsection{Verschiedene Benutzergruppen}
  %
  Vor allem bei individuell entwickelter Software ist zwar davon auszugehen, dass diese
  meist von fachkundigen Anwendern genutzt wird. Bei Software für den Massenmarkt sollte
  allerdings bedacht werden, dass sich fachliche Kompetenzen der einzelnen Benutzer teilweise
  stark unterscheiden. So gibt es sowohl Anfänger, als auch Fortgeschrittene oder Experten.
  Für all diese Benutzergruppen steht die gleiche Benutzeroberfläche zur Verfügung. Es sollte
  also darauf geachtet werden, dass sie den Anfänger nicht überfordert und er jederzeit eine
  geeignete Hilfefunktion aufrufen kann. Diese Hilfefunktionen oder auch andere unterstützende
  Designelemente sollten jedoch dezent platziert werden, damit sich erfahrenere Benutzer
  nicht daran stören.
  %
  \section{Chancen}
  %
  Aus der Integration von softwareergonomischen Gesichtspunkten in das Requirements-Engineering
  ergeben sich durchaus einige Chancen und Vorteile, sowohl für den Dienstleister, welcher die
  Software entwickelt, als auch für den Auftraggeber und die Benutzer.
  \newline
  Der Benutzer hat ein zufriedenstellendes Nutzererlebnis durch weniger Stress und einfach zu
  bedienende Software. Er ist effizienter und zudem auch motivierter.
  \newline
  Der Auftraggeber muss seine Mitarbeiter dank selbsterklärender Software nicht gesondert
  schulen. Zudem profitiert er natürlich ebenso von der gesteigerten Effizienz und Motivation
  seiner Mitarbeiter.
  \newline
  Der Dienstleister hat einen zufriedenen Kunden und ein (weiteres) ergonomisch gestaltetes
  Produkt in seinem Portfolio.
  %
  \section{Fazit}
  %
  Werden ergonomische Anforderungen von Beginn an wahrgenommen und gleichberechtigt mit den
  funktionalen Anforderungen behandelt, ergeben sich schlussendlich eine Reihe von Vorteilen
  für alle Beteiligten. Allerdings gibt es einiges zu beachten und einige Herausforderungen zu
  überstehen. So sollten von Anfang an Endnutzer der Software sowohl beim Planungsprozess als
  auch beim Testen der Software dabei sein und Feedback geben. Programmierer und Designer
  sollten in der Lage sein, dieses Feedback relativ zügig in eine nächste Iteration der
  Software umzusetzen, welche dann ebenfalls erneut getestet wird.
  \newline
  Am Ende steht eine funktionierende und gut benutzbare Software für den Einsatz bereit.
  Endnutzer und Auftraggeber können von gestiegener Effizienz und Motivation profitieren,
  die Softwareentwickler von zufriedenen Kunden.
  %
  \begin{thebibliography}{99}
    %
    \bibitem{gabler:requirements_engineering}
    Prof. Dr. Lackes R., Dr. Siepermann M.:
    Gabler Wirtschaftslexikon, Stichwort: Requirements Engineering
    \url{http://wirtschaftslexikon.gabler.de/Archiv/75983/requirements-engineering-v10.html}
    %
    \bibitem{gabler:ergonomie}
    Prof. Dr. Bartscher T.:
    Gabler Wirtschaftslexikon, Stichwort: Ergonomie
    \url{http://wirtschaftslexikon.gabler.de/Archiv/75983/requirements-engineering-v10.html}
    %
    \bibitem{h_it:software_ergonomie}
    Bräutigam L., Schneider W.:
    Projektleitfaden Software-Ergonomie
    \url{http://www.hessen-it.de/mm/software-ergonomie.pdf}
    12 (2011)
    %
    \bibitem{vo:survey}
    VERSIONONE:
    9th ANNUAL State of Agile Survey
    \url{http://www.modusnext.ch/wpcontent/uploads/Agile_Report_Annual%20State%20of%20Agile%20Survey_2015.pdf}
    2 (2015)
    %
    \bibitem{pohl:re}
    Pohl K.:
    Requirements Engineering: Fundamentals, Principles, and Techniques
    214-215 (2010)
    %
    \bibitem{edw:requirements_engineering}
    Dibbern J., Patig S.:
    Enzyklopädie der Wirtschaftsinformatik, Stichwort: Requirements Engineering
    \url{http://www.enzyklopaedie-der-wirtschaftsinformatik.de/wi-enzyklopaedie/lexikon/is-management/Systementwicklung/Hauptaktivitaten-der-Systementwicklung/Problemanalyse-/Requirements-Engineering/index.html}
    %
    \bibitem{moser:ux_design}
    Moser C.:
    User Experience Design: Mit erlebniszentrierter Softwareentwicklung zu Produkten,
    die begeistern
    46 (2012)
    %
    \bibitem{brautigam:grundwissen_se}
    Bräutigam L.:
    Grundwissen: Software-Ergonomie
    \url{http://www.sovt.de/PDF/Grundwissen_SW_Ergonomie.pdf}
    4 (1999)
    %
  \end{thebibliography}
\end{document}
